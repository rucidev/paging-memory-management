package main.java.com.epoka.paging.manager;

import main.java.com.epoka.paging.model.process.Task;
import main.java.com.epoka.paging.thread.CheckerThread;
import main.java.com.epoka.paging.thread.LoaderThread;
import main.java.com.epoka.paging.thread.ProducerThread;

import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author aleksruci on 24/Dez/2018
 */
public class SystemManager {
    public static final String TEST_FILE_PATH = "C:\\Users\\Aleks Ruci\\Downloads\\z.txt";
    public static final int MAX_PAGES_IN_MEMORY = 5;

    private static int order = 0;
    public static boolean isProducerFinished = false;
    public static boolean isCheckerFinished = false;

    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter file path name: ");
//        String filePath = scanner.nextLine();
        String filePath = TEST_FILE_PATH;

        Task task1 = new Task();
        Task task2 = new Task();

        Thread producerThread = new Thread(new ProducerThread(filePath, task1, task2), "PRODUCER THREAD");
        Thread loaderThread1 = new Thread(new LoaderThread(task1), "LOADER THREAD 1");
        Thread loaderThread2 = new Thread(new LoaderThread(task2), "LOADER THREAD 2");
        Thread checkerThread = new Thread(new CheckerThread(task1, task2), "CHECKER THREAD");

        producerThread.start();
        loaderThread1.start();
        loaderThread2.start();
        checkerThread.start();

        producerThread.join();
        System.out.println(Thread.currentThread().getName() + ": Producer Thread finished.");

        loaderThread1.join();
        System.out.println(Thread.currentThread().getName() + ": Loader Thread 1 finished.");

        loaderThread2.join();
        System.out.println(Thread.currentThread().getName() + ": Loader Thread 1 finished.");

        checkerThread.join();
        System.out.println(Thread.currentThread().getName() + ": Checker Thread finished.");

    }

    public static int getOrder() {
        return order++;
    }
}
