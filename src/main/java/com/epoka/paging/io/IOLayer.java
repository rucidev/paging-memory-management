package main.java.com.epoka.paging.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author aleksruci on 24/Dez/2018
 */
public class IOLayer {
    public static final String IO_PATH = "";

    private Scanner scanner;

    public IOLayer() {

    }

    public IOLayer(String fileName) throws FileNotFoundException{
        openConnection(fileName);
    }

    public void openConnection(String fileName) throws FileNotFoundException {
        scanner = new Scanner(new File(IO_PATH + fileName));
    }

    public void closeConnection() {
        if(isScannerOpen()) {
            scanner.close();
        }
    }

    public String next() {
        return scanner.next();
    }

    public boolean hasNext() {
        return scanner.hasNext();
    }

    private boolean isScannerOpen() {
        return scanner != null;
    }
}
