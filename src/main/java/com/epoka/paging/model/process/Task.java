package main.java.com.epoka.paging.model.process;

import main.java.com.epoka.paging.manager.SystemManager;
import main.java.com.epoka.paging.model.memory.PageBuffer;
import main.java.com.epoka.paging.model.memory.PageTable;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author aleksruci on 24/Dez/2018
 */
public class Task {
    public static final int MINUS_ONE = -1;
    public static final int MAX_PAGES_IN_BUFFER = 6;
    public static final int PAGE_TABLE_1_SIZE = 16;
    public static final int PAGE_TABLE_2_SIZE = 8;
    public static final String TASK_1_DEFAULT_ID = "T1";
    public static final String TASK_2_DEFAULT_ID = "T2";

    private String id;
    private PageTable pageTable;
    private PageBuffer pageBuffer;
    private int counter = 0;
    private Lock lock;
    private Condition condition;
    public boolean isLoaderFinished = false;

    public Task() {
        this.lock = new ReentrantLock();
        this.condition = lock.newCondition();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PageTable getPageTable() {
        return pageTable;
    }

    public void setPageTable(PageTable pageTable) {
        this.pageTable = pageTable;
    }

    public PageBuffer getPageBuffer() {
        return pageBuffer;
    }

    public void setPageBuffer(PageBuffer pageBuffer) {
        this.pageBuffer = pageBuffer;
    }

    public boolean hasFreeAllocation() {
        return !pageTable.isFull();
    }

    public void incrementCounter() {
        if(counter < SystemManager.MAX_PAGES_IN_MEMORY) {
            counter++;
        }
    }

    public void decrementCounter() {
        if(counter > 0) {
            counter--;
        }
    }

    public Lock getLock() {
        return lock;
    }

    public void setLock(Lock lock) {
        this.lock = lock;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public int getCounter() {
        return counter;
    }

    @Override
    public String toString() {
        return "[Task " + id + "]";
    }
}
