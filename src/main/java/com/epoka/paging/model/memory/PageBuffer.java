package main.java.com.epoka.paging.model.memory;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author aleksruci on 24/Dez/2018
 */
public class PageBuffer {
    private int size;
    private BlockingQueue<Page> pages;

    public PageBuffer(int size) {
        this.size = size;
        pages = new ArrayBlockingQueue<>(size);
    }

    public void addPage(Page page) throws InterruptedException{
        pages.put(page);
    }

    public boolean isFull() {
        return pages.remainingCapacity() == 0;
    }

    public boolean isEmpty() {
        return pages.isEmpty();
    }

    public Page takePage() throws InterruptedException {
        return pages.take();
    }

    public BlockingQueue<Page> getPages() {
        return pages;
    }
}
