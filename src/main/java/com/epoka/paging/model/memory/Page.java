package main.java.com.epoka.paging.model.memory;

/**
 * @author aleksruci on 24/Dez/2018
 */
public class Page {
    private int id; // logical address
    private String physicalAddress; // address in main memory
    private int loadOrder;
    private boolean isLoaded; // valid bit for pages loaded in memory

    public Page(int id, String physicalAddress) {
        this.id = id;
        this.physicalAddress = physicalAddress;
    }

    public Page(int id, String physicalAddress, int loadOrder) {
        this.id = id;
        this.physicalAddress = physicalAddress;
        this.loadOrder = loadOrder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public int getLoadOrder() {
        return loadOrder;
    }

    public void setLoadOrder(int loadOrder) {
        this.loadOrder = loadOrder;
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    public void setLoaded(boolean loaded) {
        isLoaded = loaded;
    }

    @Override
    public String toString() {
        return "[Page " + id + "-" + physicalAddress + "]";
    }
}
