package main.java.com.epoka.paging.model.memory;

import main.java.com.epoka.paging.manager.SystemManager;

import static main.java.com.epoka.paging.model.process.Task.MINUS_ONE;

/**
 * @author aleksruci on 24/Dez/2018
 */
public class PageTable {
    private int size;
    private Page[] pages;
    private int allocation;

    public PageTable(int size) {
        this.size = size;
        pages = new Page[size];
    }

    public boolean addPage(int index, Page page) {
        if(index >= 0 && index < pages.length){
            pages[index] = page;
            allocation++;
            return true;
        }
        return false;
    }

    private Page removePageByLoadOrder() {
        int maxLoadOrder = MINUS_ONE, id = MINUS_ONE;
        for(int i = 0; i < pages.length; i++) {
            Page page = pages[i];
            if(page != null && page.getLoadOrder() > maxLoadOrder) {
                maxLoadOrder = page.getLoadOrder();
                id = page.getId();
            }
        }
        if(id != MINUS_ONE) {
            Page returnPage = pages[id];
            pages[id] = null;
            return returnPage;
        }
        return null;
    }

    public boolean isFull() {
        return allocation >= SystemManager.MAX_PAGES_IN_MEMORY;
    }

    public Page removePage() {
        allocation--;
        return removePageByLoadOrder();
    }

    public Page[] getPages() {
        return pages;
    }
}
