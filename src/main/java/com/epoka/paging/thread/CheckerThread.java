package main.java.com.epoka.paging.thread;

import main.java.com.epoka.paging.manager.SystemManager;
import main.java.com.epoka.paging.model.memory.Page;
import main.java.com.epoka.paging.model.process.Task;

/**
 * @author aleksruci on 24/Dez/2018
 */
public class CheckerThread implements Runnable {
    private Task task1;
    private Task task2;

    public CheckerThread(Task task1, Task task2) {
        this.task1 = task1;
        this.task2 = task2;
    }

    @Override
    public void run() {
        while (!(SystemManager.isProducerFinished && task1.isLoaderFinished && task2.isLoaderFinished)) {
            checkTask(task1);
            checkTask(task2);
        }
        SystemManager.isCheckerFinished = true;
        System.out.println("Checker finished");
    }

    private void checkTask(Task task) {
        if (task.getPageTable() != null && !task.hasFreeAllocation()) {
            try {
                task.getLock().lock();
                Page removedPage = task.getPageTable().removePage();
                task.decrementCounter();
                System.out.println(Thread.currentThread().getName() + ": " + task + " removed " + removedPage);
                task.getCondition().signalAll();
            } finally {
                task.getLock().unlock();
            }
        }
    }
}
