package main.java.com.epoka.paging.thread;

import main.java.com.epoka.paging.manager.SystemManager;
import main.java.com.epoka.paging.model.process.Task;
import main.java.com.epoka.paging.model.memory.Page;

/**
 * @author aleksruci on 24/Dez/2018
 */
public class LoaderThread implements Runnable{
    private Task task;

    public LoaderThread(Task task) {
        this.task = task;
    }

    @Override
    public void run() {
        try {
            while(!(SystemManager.isProducerFinished && task.getPageBuffer().isEmpty())) {
                waitForPageInBuffer();
                Page page = task.getPageBuffer().takePage();
                task.getLock().lock();
                waitForSpaceInTable(page);
                page.setLoadOrder(SystemManager.getOrder());
                task.getPageTable().addPage(page.getId(), page);
                task.incrementCounter();
                System.out.println(Thread.currentThread().getName() + ": Page Table of " + task + " added " + page + " with order number " + page.getLoadOrder());
                task.getLock().unlock();
            }
            task.isLoaderFinished = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void waitForSpaceInTable(Page page) throws InterruptedException {
        if(!task.hasFreeAllocation()) {
            System.out.println(Thread.currentThread().getName() + ": " + task + " currently has no allocation for " + page);
            task.getCondition().await();
        }
    }

    private void waitForPageInBuffer() {
        System.out.println(Thread.currentThread().getName() + ": " + task + " waiting for page allocation in buffer.");
        while((task.getPageBuffer() == null || task.getPageBuffer().isEmpty())){ }
        System.out.println(Thread.currentThread().getName() + ": " + task + " stopped waiting for pages in buffer.");
    }
}
