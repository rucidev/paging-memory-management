package main.java.com.epoka.paging.thread;

import main.java.com.epoka.paging.io.IOLayer;
import main.java.com.epoka.paging.manager.SystemManager;
import main.java.com.epoka.paging.model.memory.PageBuffer;
import main.java.com.epoka.paging.model.memory.PageTable;
import main.java.com.epoka.paging.model.process.Task;
import main.java.com.epoka.paging.model.memory.Page;

import java.io.FileNotFoundException;

import static main.java.com.epoka.paging.model.process.Task.PAGE_TABLE_1_SIZE;
import static main.java.com.epoka.paging.model.process.Task.PAGE_TABLE_2_SIZE;
import static main.java.com.epoka.paging.model.process.Task.MAX_PAGES_IN_BUFFER;

/**
 * @author aleksruci on 24/Dez/2018
 */
public class ProducerThread implements Runnable{
    private IOLayer io;
    private Task task1;
    private Task task2;

    public ProducerThread(String filePath, Task task1, Task task2) throws FileNotFoundException {
        io = new IOLayer(filePath);
        this.task1 = task1;
        this.task2 = task2;
    }

    @Override
    public void run() {
        while(io.hasNext()) {
            String taskId = io.next().substring(0, 2);
            int pageId = Integer.parseInt(io.next().substring(0, 1));
            String physicalAddress = io.next();
            try {
                if (task1.getId() == null) {
                    initializeTask(taskId, task1, PAGE_TABLE_1_SIZE);
                } else if (task2.getId() == null) {
                    initializeTask(taskId, task2, PAGE_TABLE_2_SIZE);
                } else {
                    Page page = new Page(pageId, physicalAddress);
                    if (task1.getId().equals(taskId)) {
                        addPageToTaskBuffer(page, task1);
                    } else if (task2.getId().equals(taskId)) {
                        addPageToTaskBuffer(page, task2);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        SystemManager.isProducerFinished = true;
    }

    private void addPageToTaskBuffer(Page page, Task task1) throws InterruptedException {
        task1.getPageBuffer().addPage(page);
        System.out.println(Thread.currentThread().getName() + ": " + "Buffer of " + task1 + " now with size " + task1.getPageBuffer().getPages().size() + " added " + page);
    }

    private void initializeTask(String taskId, Task task1, int pageTable1Size) {
        task1.setId(taskId);
        task1.setPageTable(new PageTable(pageTable1Size));
        task1.setPageBuffer(new PageBuffer(MAX_PAGES_IN_BUFFER));
        System.out.println(Thread.currentThread().getName() + ": " + task1 + " instansiated.");
    }
}
